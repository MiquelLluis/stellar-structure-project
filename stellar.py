# /usr/bin/env python3
#
# Developed by
#   Andreu Sales Masó Ferrando (anmafe@alumni.uv.es)
#   Miquel Lluís Llorens Monteagudo (millomon@alumni.uv.es)
# as a part of the final project of the Stellar Astrophysics subject.
#
# Master's degree: Advanced Physics
# Universitat de València (https://www.uv.es/)
# 2017 - 2018

import io
import time
import subprocess
import multiprocessing as mpc

import numpy as np
import scipy as sp

from scipy.interpolate import interp1d
from matplotlib import pyplot as plt
from astropy import constants as ct


T_SUN = 1.442e7   # K
P_SUN = 1.482e17  # Ba
R_SUN = ct.R_sun.cgs.value  # cm
L_SUN = ct.L_sun.cgs.value  # erg/s
M_SUN = ct.M_sun.cgs.value  # g


def def_n(m0, m1, n_pp, n_cno):
    """
    Closure for calculating the nuclear energy generation exponent 'n' in
    arbitrary mass ranges of PP-CNO cycles:
                / n_pp         | if m < m0
        n(m) = {  lin. interp. | if m0 < m < m1
                \ n_cno        | if m > m1
    'm' is in solar masses.

    """
    a = (n_cno - n_pp) / (m1 - m0)
    b = n_pp - m0 * a

    def nfun(m):
        if m < m0:
            return n_pp
        elif m0 < m < m1:
            return a * m + b
        return n_cno
    nfun = np.vectorize(nfun, otypes=[float])

    return nfun


def fun_rad(m, n):
    """R/Rsun (M/Msun, n)"""
    return m ** ((n-1) / (n+3))


def fun_lum(m):
    """L/Lsun (M/Msun, n)"""
    return m ** 3.5


def fun_temp(m, n):
    """T/Tsun (M/Msun, n)"""
    return m ** (4 / (3+n))


def fun_pres(m, n):
    """P/Psun (M/Msun, n)"""
    return m ** (2 * (5-n) / (3+n))


def model_configs(mass, x, y, n):
    """Configuration parameters (M, X, Y, Pc, Tc, R, L) for `zams`."""
    configs = np.empty((7, len(mass)), dtype=float)
    configs[0] = mass
    configs[1] = x
    configs[2] = y
    configs[3] = np.log10(fun_pres(mass, n) * P_SUN)
    configs[4] = np.log10(fun_temp(mass, n) * T_SUN)
    configs[5] = np.log10(fun_rad(mass, n) * R_SUN)
    configs[6] = np.log10(fun_lum(mass) * L_SUN)
    
    return configs


def interp_configs(mass, x, y, omass, ostars):
    """Configuration parameters (M, X, Y, Pc, Tc, R, L) interpolated from a
    previous 'build_stellar_population' result.

    PARAMETERS
    ----------
    mass: array-like
        New masses to compute (in solar masses)
    
    x, y: float
        Metallicity fractions.

    omass: array-like
        Original masses (in solar masses) for interpolating.

    ostars: array, 'build_stellar_population's output
        Original stellar results for interpolating.
    
    """
    mass = np.atleast_1d(mass)
    _kwargs = {'kind':'quadratic', 'fill_value':'extrapolate'}

    # Ignore stars with NaN values
    ind = np.logical_and.reduce([~np.isnan(ostars[:,i,0]) for i in (2,3,4,6)])

    # We compute the interpolation with logarithmic masses log10(M/M_sun)
    _log_mass = np.log10(mass)
    _log_omass = np.log10(omass[ind])

    configs = np.empty((7, len(mass)), dtype=float)
    configs[0] = mass
    configs[1] = x
    configs[2] = y
    configs[3] = interp1d(_log_omass, ostars[ind,3,0], **_kwargs)(_log_mass)
    configs[4] = interp1d(_log_omass, ostars[ind,4,0], **_kwargs)(_log_mass)
    configs[5] = interp1d(_log_omass, ostars[ind,2,-1], **_kwargs)(_log_mass)
    configs[6] = interp1d(_log_omass, ostars[ind,6,-1], **_kwargs)(_log_mass)

    return configs


def zams(mass, xmet, ymet, press0, temp0, rad, lum, ofile=None, opfile=None,
         load_all=False, mode=0):
    """Interface for the compiled ZAMS program.

    PARAMETERS
    ----------
    mass: array
        Mass in solar masses.
    xmet, ymet: float
        Metallicity fractions.
    press0, temp0, rad, lum: float
        Guesses for central pressure, central temperature, total radius and
        total luminosity. All in decimal logarithmic scale (of absolute values).
    ofile, opfile: str, optional
        Directories where to save ZAMS outputs
    load_all: bool, optional
        If True, includes nuclear generation EPS, opacity, Lc, Lc/Ltot, Delta,
        adiabatic Delta, and radiation Delta.
    mode: int, optional
        Available modes:
            0-> Opacity x 1 (original)
            1-> Opacity x 0.5
            2-> Opacity x 2
            3-> Replace EPS output by EPSPP (only PP component)
    
    RETURNS
    -------
    star: array, shape (7, 199) if not 'load_all', else (14, 199)
        Each of the profiles, inside out:
         0      1        2           3          4           5          6
        [N,  1-Mr/M,  log10(r),  log10(p),  log10(t),  log10(rho),  log10(l)]
        
        if 'load_all', also:
             7           8         9         10      11      12         13
        [log10(EPS), log10(k), log10(Lc), Lc/Ltot, Delta, Delta_ad, Delta_rad]
    
    """
    # Parameter conversion
    ofile = ofile if ofile is not None else "/dev/shm/zams.out"
    args = (
        mass,
        "%f %f" % (xmet, ymet),
        10 ** press0,
        10 ** temp0,
        10 ** rad,
        10 ** lum / L_SUN,
        ofile,
        *(('Y', opfile) if opfile is not None else 'N')
    )
    args = "\n".join(str(a) for a in args).encode()
    
    # Process call
    process = subprocess.Popen(
        {0: './zams', 1: './zams_05op', 2: './zams_2op', 3: './zams_eps'}[mode],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE
    )
    process.communicate(args)
    
    # Import data
    with open(ofile, "rb") as f:
        buff = f.read()
    p0 = buff.find(b"LOG(L)\n") + 7
    p1 = buff.find(b"\n ***************************************")
    if p1 == -1:
        return None
    
    data = buff[p0:p1]
    data = data.replace(b"D-", b"e-").replace(b"D+", b"e+")

    if load_all:
        star = np.empty((14, 199))
        star[:7] = np.genfromtxt(io.BytesIO(data), delimiter=(5,19,9,8,8,8,8),
                                 unpack=True)

        # Imports additional data
        p0 = buff.find(b"DELRAD\n") + 7
        data = buff[p0:]
        data = data.replace(b"D-", b"e-").replace(b"D+", b"e+")

        star[7:] = np.genfromtxt(io.BytesIO(data), delimiter=(4,8,8,8,8,8,8,14),
                                 usecols=range(1,8), unpack=True)
    else:
        star = np.loadtxt(io.BytesIO(data), unpack=True)

    return star


def _build_stellar_population(configs, n_jobs=8, load_all=False, mode=0):
    """Builds a stellar population composed of masses inside 'configs'."""
    n = configs.shape[-1]
    n_succ = 0  # Number of successful simulations
    
    # Parallel processing
    i = 0  # completed profiles
    j = 0  # profiles being computed
    q = mpc.Queue(maxsize=n_jobs)
    res = []
    while i < n:
        # Always keep 'n_jobs' alive until all stars are generated
        while j < n_jobs and i+j < n:
            p = mpc.Process(
                target=(lambda q, *args, **kwargs:
                        q.put((i+j, zams(*args, **kwargs)))),
                args=(q, *configs[:,i+j]),
                kwargs={'ofile': "/dev/shm/zams%02d.out" % (i+j),
                        'load_all': load_all, mode: mode}
            )
            p.start()
            j += 1
        time.sleep(0.2)
        # Dynamically retrieve results when available
        while not q.empty():
            res.append(q.get())
            if res[-1][1] is not None:
                n_succ += 1
            i += 1
            j -= 1
            print("Computed stars: %2d/%d | Success: %2d" % (i, n, n_succ),
                  end='\r', flush=True)
    res.sort(key=lambda x: x[0])  # keep the same order as in 'configs'
    res = [res_i[1] for res_i in res]

    # Filter and rearrange the results in a numpy array
    stars = np.empty((n, 7 if not load_all else 14, 199), dtype=float)
    i_succ = [] # Indices of successful simulations
    for i in range(len(res)):
        if res[i] is not None:
            stars[i] = res[i]
            i_succ.append(i)
        else:
            stars[i] = np.nan
    
    return stars, i_succ


def build_stellar_population(configs, masses=None, max_iter=10, n_jobs=None,
                             load_all=False, mode=0):
    """Builds a stellar population composed of masses inside 'configs'.

    If 'max_iter' > 1 and not all configurations are successfully solved,
    the stellar population is built again using the interpolated/extrapolated
    results as initial conditions recursively.

    PARAMETERS
    ----------
    configs: array
        Configuration parameters.

    masses: array-like, optional
        If provided, recompute the stellar profiles for the new 'masses' after
        reaching the convergence of given 'configs'. Useful for computing a
        large amount of stellar configurations within the given 'configs'
        whithout having to iterate over all of them.

    max_iter: int, optional
        Maximum number of iterations in case of not succeeding all
        configurations in 'configs'. Defaults to 10.

    n_jobs: int, optional
        Number of star configurations to compute simultaneously.
        A safe value is the number of CPU cores. Defaults to it.

    load_all: bool, optional
        (See 'zams' function)

    mode: int, optional
        (See 'zams' function)

    """
    if n_jobs is None:
        n_jobs = mpc.cpu_count()
    stars, i_succ = _build_stellar_population(configs, n_jobs=n_jobs,
                                              load_all=load_all)

    # Recursive iteration between 'stars' and 'configs'
    i = 1
    last_i_succ = []

    while (i < max_iter and len(i_succ) < configs.shape[1]
                        and len(i_succ) > len(last_i_succ)):
        print(" "*60, end='\r', flush=True) # Cleans last print

        last_i_succ = i_succ.copy()

        configs = interp_configs(
            configs[0],  # could be improved avoiding recalculations
            configs[1,0],
            configs[2,0],
            configs[0,last_i_succ],
            stars[last_i_succ]
        )
        stars, i_succ = _build_stellar_population(configs, n_jobs=n_jobs,
                                                  load_all=load_all)

        i += 1

    # Raise an exception if all mass points failed to converge
    if len(i_succ) == 0:
        raise ValueError("No solution found for the given parameters.")
    
    # Optional resample of stellar population with different 'masses'.
    if masses is not None:
        configs = interp_configs(
            masses,
            configs[1,0],
            configs[2,0],
            configs[0,i_succ],
            stars[i_succ]
        )
        stars, i_succ = _build_stellar_population(configs, n_jobs=n_jobs,
                                                  load_all=load_all)

    print()
    if len(i_succ) < configs.shape[-1]:
        print("WARNING: Zams did not converge for all stellar configurations.")
    print("Process finished after %d iterations." % i)

    return stars


def plot_population_profiles(ax, masses, stars, configs=None, **kwargs):
    """Plots Pc, Tc, R, L (input & output) of 'build_stellar_population'.""" 
    if configs is not None:  
        mass0 = configs[0]
        pres0 = 10 ** configs[3]
        temp0 = 10 ** configs[4]
        rads0 = 10 ** configs[5]
        lums0 = 10 ** configs[6] / L_SUN

    pres = 10 ** stars[:,3,0]  # Pc
    temp = 10 ** stars[:,4,0]  # Tc
    rads = 10 ** stars[:,2,-1] # R
    lums = 10 ** stars[:,6,-1] / L_SUN  # L

    # P, T, R, L
    paths = (
        ax[0,0].scatter(masses, pres, s=6, **kwargs),
        ax[0,1].scatter(masses, temp, s=6, **kwargs),
        ax[1,0].scatter(masses, rads, s=6, **kwargs),
        ax[1,1].scatter(masses, lums, s=6, **kwargs)
    )

    if configs is not None:
        lines = (
            ax[0,0].plot(mass0, pres0)[0],
            ax[0,1].plot(mass0, temp0)[0],
            ax[1,0].plot(mass0, rads0)[0],
            ax[1,1].plot(mass0, lums0)[0]
        )
    
    # Configs
    ax[0,0].set_ylabel('$P_C$ [Ba]')
    ax[0,1].set_ylabel('$T_C$ [K]')
    ax[1,0].set_ylabel('$R$ [cm]')
    ax[1,1].set_ylabel('$L$ [$L_\odot$]')
    
    for i in range(2):
        for j in range(2):
            ax[i,j].set_xscale('log')
            ax[i,j].set_yscale('log')
            ax[i,j].set_xlabel('M [$M_\odot$]')

    return paths if configs is None else (paths, lines)

def radiance (temp, wl):
    return ((2* 6.626e-27*(3e8)**2)/(wl)**5) *(1/(np.exp((6.626e-27*3e8)/(wl*1.38e-16*temp))-1))

def ksi(M):#Salpeter function 
    # M in solar masses
    return 3178 * M**-2.35